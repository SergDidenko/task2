<?php

add_action( 'widgets_init', 'my_widget' );


function my_widget() {
	register_widget( 'MY_Widget' );
}

class MY_Widget extends WP_Widget {

	function MY_Widget() {
		$widget_ops = array( 'classname' => 'event', 'description' => __('A widget that displays events and date ', 'event') );
		
		$control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'event-widget' );
		
		$this->WP_Widget( 'event-widget', 'Event Widget', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$arg=array('post_type'=>'event',
				   'meta_value'=>$instance['status'],
				   'posts_per_page'=>$instance['quantity']
		);
		$posts = get_posts( $arg );
		echo $before_widget;
		foreach ( $posts as $post ) {
			if ( $instance['status'] == $post->select_status) {
				echo "<h5>The name of event : ".$post->post_title."</h5>";
				echo "<h5>The date of event is: ".$post->date."</h5>";
				echo "<hr>";
			}else{
				continue;
			}
		}
		echo $after_widget;
		
	}

	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML 
		$instance['quantity'] = strip_tags( $new_instance['quantity'] );
		$instance['status'] = strip_tags( $new_instance['status'] );
		$instance['show_info'] = $new_instance['show_info'];

		return $instance;
	}

	
	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'quantity' => '', 'status' => '', 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'quantity' ); ?>"><?php echo 'Quantity of events:'; ?></label>
			<input id="<?php echo $this->get_field_id( 'quantity' ); ?>" name="<?php echo $this->get_field_name( 'quantity' ); ?>" value="<?php echo $instance['quantity']; ?>" style="width:100%;" />

		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'status' ); ?>"><?php echo 'Status:'; ?></label>
			<select name="<?php echo $this->get_field_name( 'status' ); ?>" id="<?php echo $this->get_field_id( 'status' ); ?>">
				<option value="Opened_event">Opened_event</option>
				<option value="With_invitation_card">With_invitation_card</option>
			</select>		
		</p>

	<?php
	}
}

?>