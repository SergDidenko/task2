<?php 
	add_shortcode('shortcode_event', 'create_shortcode_event');
	function create_shortcode_event($atts){
		extract(shortcode_atts(['quantity'=>5, 'status'=>'Opened event'], $atts));
		$args=array( 'post_type'=>'event',
					 'meta_value'=>$status,
					 'posts_per_page'=>$quantity
		);
		$posts=get_posts($args);
		$res='';
		foreach ( $posts as $post ) {
			$res.="<h5>The name of event : ".$post->post_title."</h5><br><h5>The date of event is: ".$post->date."</h5>";
		}
		return $res;
	}

?>
