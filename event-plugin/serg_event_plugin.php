<?php
/*
Plugin Name: Serg Event Plugin
Plugin URI: http://localhost/events-info
Description: Creation of post type
Version: 1.0
Author: Sergey
Author URI: 
*/

// Our custom post type function

function create_posttype() {

	//settings for register_post_type

	$labels = array(
		'name'               => 'Events',
		'singular_name'      => 'Event',
		'menu_name'          => 'Events', 
		'name_admin_bar'     => 'Event', 
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Event',
		'new_item'           => 'New Event',
		'edit_item'          => 'Edit Event',
		'view_item'          => 'View Event',
		'all_items'          => 'All Events',
		'search_items'       => 'Search Events',
		'parent_item_colon'  => 'Parent Events:',
		'not_found'          => 'No events found.',
		'not_found_in_trash' => 'No events found in Trash.'
	);

	$args = array(
		'labels'             => $labels,
        'description'        => 'Plugin  should create new post type with different additional parameters',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array('slug' => 'event'),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 10,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'custom fields' ),
		'taxonomies'=> array('event_category')
	);
	register_post_type( 'event', $args );
}
// Init is the hook which we use.
add_action('init', 'create_taxonomy');
add_action('init', 'create_posttype');

function create_taxonomy(){

	$labels = array(
		'name'              => 'Categories',
		'singular_name'     => 'Category',
		'search_items'      => 'Search Categories',
		'all_items'         => 'All Categories',
		'parent_item'       => 'Parent Category',
		'parent_item_colon' => 'Parent Category:',
		'edit_item'         => 'Edit Category',
		'update_item'       => 'Update Category',
		'add_new_item'      => 'Add New Category',
		'new_item_name'     => 'New Genre Category',
		'menu_name'         => 'Category',
	); 
	// параметры
	$args = array(
		'label'                 => '', 
		'labels'                => $labels,
		'description'           => '', 
		'public'                => true,
		'publicly_queryable'    => null, 
		'show_in_nav_menus'     => true, 
		'show_ui'               => true, 
		'show_tagcloud'         => true, 
		'show_in_rest'          => null,
		'rest_base'             => null,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
	);
	register_taxonomy('event_category', 'event', $args);
}

function event_meta_box() {  
    add_meta_box(  
        'event_meta_box',
        'Event - additional information',
        'show_my_event_metabox',
        'event',
        'normal',
        'high');
}  
add_action('add_meta_boxes', 'event_meta_box');

$event_meta_fields = array(
    array(  
        'label' => 'Status',  
        'desc'  => 'Choose status.',  
        'id'    => 'select_status',  
        'type'  => 'select',  
        'options' => array (  
            'one' => array (  
                'label' => 'Opened_event',  
                'value' => 'Opened_event' 
            ),  
            'two' => array (  
                'label' => 'With_invitation_card',
                'value' => 'With_invitation_card'
            ),  
        )
    ), 
    array(  
        'label' => 'Date',  
        'desc'  => '',  
        'id'    => 'date', 
        'type'  => 'date' 
    )
);

function show_my_event_metabox() {  
	global $event_meta_fields;
	global $post;
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    echo '<table class="form-table">';  
    foreach ($event_meta_fields as $field) {  
        $meta = get_post_meta($post->ID, $field['id'], true);  
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';  
                switch($field['type']) {  
					case 'date':  
					    echo '<input type="date" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>';
}

function save_my_event_meta_fields($post_id) {  
    global $event_meta_fields;
 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))  
        return $post_id;  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    if ('event' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    foreach ($event_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new); 
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);
        }  
    }
}  
add_action('save_post', 'save_my_event_meta_fields'); 
require "widgets/simple-widget.php";
require "shortcode-event.php";

